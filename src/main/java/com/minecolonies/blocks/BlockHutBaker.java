package com.minecolonies.blocks;

/**
 * Hut for the baker.
 * No different from {@link AbstractBlockHut}
 */
public class BlockHutBaker extends AbstractBlockHut
{
    protected BlockHutBaker()
    {
        //No different forom Abstract parennt
        super();
    }

    @Override
    public String getName()
    {
        return "blockHutBaker";
    }
}
